﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SZK.HomeManagement.Shared.Constants
{
    public enum ProductCategory
    {
        Meat,
        Other,
        Drink,
        Dairy,
        Fat,
        Vegetable,
        Fruit,
        Grain
    }
}
