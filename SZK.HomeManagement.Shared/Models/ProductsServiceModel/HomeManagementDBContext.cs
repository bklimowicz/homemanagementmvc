﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using SZK.HomeManagement.Shared.Config;

namespace SZK.HomeManagement.Shared.Models.ProductsServiceModel
{
    public class HomeManagementDBContext : DbContext
    {
        public DbSet<Product> Product { get; set; }
        public DbSet<ShoppingListItem> ShoppingListItem { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            DatabaseConfiguration dbConfig = new DatabaseConfiguration();
            optionsBuilder.UseSqlServer($@"Server={dbConfig.Server},{dbConfig.Port}
                    ;Initial Catalog={dbConfig.DatabaseName}
                    ;Persist Security Info=False
                    ;User ID={dbConfig.User}
                    ;Password={dbConfig.Password}
                    ;MultipleActiveResultSets=False
                    ;Encrypt=True
                    ;TrustServerCertificate=False
                    ;Connection Timeout=30;");
        }
    }
}
