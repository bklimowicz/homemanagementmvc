﻿using SZK.HomeManagement.Shared.Constants;

namespace SZK.HomeManagement.Shared.Models.ProductsServiceModel
{
    public class Product
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public ProductCategory ProductCategory { get; set; }
        public string Producer { get; set; }
        public Product() { }


    }
}
