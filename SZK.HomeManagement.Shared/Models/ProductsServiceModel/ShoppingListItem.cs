﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SZK.HomeManagement.Shared.Models.ProductsServiceModel
{
    public class ShoppingListItem
    {
        public int ShoppingListItemId { get; set; }
        [ForeignKey("ProductId")]
        public Product Product { get; set; }
        public int Quantity { get; set; }
        public string Description { get; set; }
        public string Requester { get; set; }
        public DateTime RequestedOn { get; set; }
        public ShoppingListItem() { }
    }
}
