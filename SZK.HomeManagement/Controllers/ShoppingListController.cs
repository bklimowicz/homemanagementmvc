﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using HomeManagement.Models;
using System.Net.Http;
using SZK.HomeManagement.Services;
using SZK.HomeManagement.Shared.Models.ProductsServiceModel;
using SZK.HomeManagement.Shared.Constants;

namespace HomeManagement.Controllers
{
    public class ShoppingListController : Controller
    {
        private readonly ILogger<ShoppingListController> _logger;
        private readonly ProductsServiceClient productsServiceClient;

        public ShoppingListController(ILogger<ShoppingListController> logger, ProductsServiceClient productsServiceClient)
        {
            _logger = logger;
            this.productsServiceClient = productsServiceClient;
        }

        public async Task<IActionResult> Index()
        {
            ViewBag.ShoppingList = await this.productsServiceClient.GetShoppingListAsync();
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task<IActionResult> AddNewShoppingListItem(string productCategoryString, string productName, int quantity, string requester, string description, string producer)
        {
            if (!Enum.TryParse(productCategoryString, out ProductCategory productCategory))
            {
                productCategory = ProductCategory.Other;
            }
            ShoppingListItem shoppingListItem = new ShoppingListItem()
            {
                Product = new Product()
                {
                    Name = productName,
                    ProductCategory = productCategory,
                    Producer = producer
                },
                Quantity = quantity,
                Description = description,
                Requester = requester,
                RequestedOn = DateTime.Now
            };

            await this.productsServiceClient.AddShoppingListItem(shoppingListItem);

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> RemoveItemFromList(int id)
        {            
            await this.productsServiceClient.RemoveItemFromList(id);
            return RedirectToAction("Index");
        }
    }
}
