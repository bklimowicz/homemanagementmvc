﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using HomeManagement.Models;
using System.Reflection.Metadata;
using SZK.HomeManagement.Shared.Constants;
using Microsoft.VisualBasic;

namespace HomeManagement.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<ShoppingListController> _logger;        

        public HomeController(ILogger<ShoppingListController> logger)
        {
            this._logger = logger;            
        }

        public IActionResult Index()
        {            
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
