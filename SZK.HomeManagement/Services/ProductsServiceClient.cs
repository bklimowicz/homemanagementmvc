﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using SZK.HomeManagement.Shared.Models.ProductsServiceModel;

namespace SZK.HomeManagement.Services
{
    public class ProductsServiceClient
    {
        private readonly HttpClient httpClient;
        private readonly string EndpointUrl = "https://szk-homemanagement-productsservice.azurewebsites.net/ShoppingList";
        public ProductsServiceClient(IHttpClientFactory httpClientFactory)
        {
            this.httpClient = httpClientFactory.CreateClient();
        }

        public async Task<List<ShoppingListItem>> GetShoppingListAsync()
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, EndpointUrl);
            try
            {

                HttpResponseMessage response = await httpClient.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    List<ShoppingListItem> shoppingList = await response.Content.ReadFromJsonAsync<List<ShoppingListItem>>();
                    return shoppingList;
                }

                return new List<ShoppingListItem>();
            }
            catch
            {
                return new List<ShoppingListItem>();
            }
        }

        public async Task AddShoppingListItem(ShoppingListItem shoppingListItem)
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, EndpointUrl)
            {
                Content = new StringContent(JsonConvert.SerializeObject(shoppingListItem), Encoding.UTF8, "application/json")
            };

            HttpResponseMessage response = await httpClient.SendAsync(request);
        }

        public async Task RemoveItemFromList(int id)
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Delete, EndpointUrl)
            {
                Content = new StringContent(JsonConvert.SerializeObject(id), Encoding.UTF8, "application/json")
            };

            HttpResponseMessage response = await httpClient.SendAsync(request);
        }
    }
}
