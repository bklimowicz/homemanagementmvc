﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SZK.HomeManagement.Shared.Models.ProductsServiceModel;
using SZK.HomeManagement.Shared.Constants;
using System.Text.Json.Serialization;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;

namespace SZK.HomeManagement.ProductsService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ShoppingListController : ControllerBase
    {
        //public static List<ShoppingListItem> ShoppingListItems = new List<ShoppingListItem>()
        //{
        //    new ShoppingListItem()
        //    {                
        //        Name = "testName1",
        //        Description = "testDescription1",
        //        ProductCategory = ProductCategory.Dairy,
        //        Producer = "",
        //        RequestedOn = new DateTime(2020,9,1),
        //        Requester = "Bartek"
        //    },
        //    new ShoppingListItem()
        //    {
        //        Name = "testName2",
        //        Description = "testDescription2",
        //        ProductCategory = ProductCategory.Dairy,
        //        Producer = "",
        //        RequestedOn = new DateTime(2020,9,1),
        //        Requester = "Filip"
        //    },
        //    new ShoppingListItem()
        //    {
        //        Name = "testName3",
        //        Description = "testDescription3",
        //        ProductCategory = ProductCategory.Dairy,
        //        Producer = "",
        //        RequestedOn = new DateTime(2020,9,1),
        //        Requester = "Adam"
        //    },
        //    new ShoppingListItem()
        //    {
        //        Name = "testName4",
        //        Description = "testDescription4",
        //        ProductCategory = ProductCategory.Dairy,
        //        Producer = "",
        //        RequestedOn = new DateTime(2020,9,1),
        //        Requester = "Ania"
        //    }
        //};

        private readonly ILogger<ShoppingListController> _logger;
        private readonly HomeManagementDBContext dBContext;

        public ShoppingListController(ILogger<ShoppingListController> logger, HomeManagementDBContext dBContext)
        {
            this._logger = logger;
            this.dBContext = dBContext;
        }

        [HttpGet]
        public IEnumerable<ShoppingListItem> Get()
        {
            return this.dBContext.ShoppingListItem.Include(x => x.Product).ToList();
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] object shoppingListItemJson)
        {
            ShoppingListItem shoppingListItem = JsonConvert.DeserializeObject<ShoppingListItem>(shoppingListItemJson.ToString());

            Product product = shoppingListItem.Product;

            if (this.dBContext.Product.FirstOrDefault(x => x.Name == product.Name
                && x.Producer == product.Producer
                && x.ProductCategory == product.ProductCategory) is Product existingProduct)
            {
                shoppingListItem.Product = existingProduct;
            }
            else
            {
                this.dBContext.Product.Add(product);
            }

            this.dBContext.ShoppingListItem.Add(shoppingListItem);
            await this.dBContext.SaveChangesAsync();

            return Ok();
        }

        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody] object id)
        {
            int shoppingListItemId = JsonConvert.DeserializeObject<int>(id.ToString());

            if (this.dBContext.ShoppingListItem.FirstOrDefault(x => x.ShoppingListItemId == shoppingListItemId) is ShoppingListItem shoppingListItem)
            {
                this.dBContext.ShoppingListItem.Remove(shoppingListItem);
                await this.dBContext.SaveChangesAsync();
                return Ok();
            }
            // Likely wont even occure :^)
            return NotFound();
        }
    }
}
